Licata Law is a full-service law firm providing legal services to both individuals and businesses across Ontario. Our expertise spans a variety of practices areas from business & franchise law, to Italian, real estate law, and family law.

Address: 45 Sheppard Ave E, #915, Toronto, ON M2N 5W9, Canada

Phone: 647-405-0470

Website: https://licatalawoffice.com
